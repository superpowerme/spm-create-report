#!/usr/bin/env python3
import datetime
import os
import sys
import mysql.connector
import requests
from dotenv import load_dotenv
from loguru import logger

load_dotenv()


class DbConnection:
    db_host = "superpowerme.it"
    db_user = os.environ["REMOTEDBUSER"]
    db_password = os.environ["PASS"]
    db_name = os.environ["DB"]

    def __init__(self):
        self.connection = mysql.connector.connect(
            host=DbConnection.db_host,
            user=DbConnection.db_user,
            password=DbConnection.db_password,
            database=DbConnection.db_name,
        )

    def query(self, sql, args):
        cursor = self.connection.cursor()
        cursor.execute(sql, args)
        return cursor

    def fetchone(self, sql, args):
        row = None
        cursor = self.query(sql, args)
        if cursor.with_rows:
            row = cursor.fetchone()
        cursor.close()
        return row

    def fetch(self, sql, args):
        rows = []
        cursor = self.query(sql, args)
        if cursor.with_rows:
            rows = cursor.fetchall()
        cursor.close()
        return rows


class Patient:
    """La classe che regitra tutti i dati relativi a ciascun
    paziente"""

    db_connection = DbConnection()

    available_heroes = {1: "Beck", 2: "Greta", 3: "Johnny 6", 4: "Andrea"}

    def __init__(
        self,
        fullname: str,
        doctor_id: int,
        id: int,
    ) -> None:
        self._fullname = fullname
        self._doctor_id = doctor_id
        self._db_id = id
        self._db_connection = Patient.db_connection
        self._game_id = self._get_game_id()
        self._coins = self._get_coins()
        self._hero = self._get_current_hero()
        self._last_action_at = self._get_last_action_time()
        self._quests = QuestContainer(self._db_id, self._game_id, self._db_connection)
        self._wear_time = None

    def __repr__(self):
        return f"{self._doctor_id}-{self._fullname}"

    def _get_game_id(self) -> int:
        query = f"SELECT id FROM games WHERE user_id = {self._db_id}"
        query_results = self._db_connection.fetchone(query, args=None)
        self._game_id = query_results[0]
        logger.debug(f"{self.__repr__()} ha il gioco n. {self._game_id}")
        return self._game_id

    def _get_coins(self) -> int:
        query = f"SELECT coins FROM games WHERE user_id = {self._db_id}"
        query_results = self._db_connection.fetchone(query, args=None)
        self._coins = query_results[0]
        logger.debug(f"{self._coins} monete per {self.__repr__()}")
        return self._coins

    def _get_current_hero(self) -> str:
        query = f"""SELECT
                      selected_hero_id
                    FROM
                      games
                    WHERE
                      user_id = {self._db_id}"""
        query_results = self._db_connection.fetchone(query, args=None)
        self._hero = Patient.available_heroes.get(query_results[0])
        logger.debug(f"{self.__repr__()} al momento gioca con {self._hero}")
        return self._hero

    def _get_last_action_time(self) -> datetime.datetime:
        query_text = f"""SELECT
                           updated_at
                         FROM
                           games
                         WHERE
                           user_id = {self._db_id}"""
        query_results = self._db_connection.fetchone(query_text, args=None)
        self._last_action_at = query_results[0]
        logger.debug(f"{self.__repr__()} last actiont at: {self._last_action_at}")
        return self._last_action_at


class QuestContainer:
    """contenitore per le quest"""

    def __init__(self, user_id, game_id, connection):
        self._user_id = user_id
        self._game_id = game_id
        self._db_connection = connection
        self._ids = None
        self._names = None
        self._times = None
        self._states = None
        self._data = None

    def _get_ids(self) -> tuple:
        query_text = f"""SELECT
                           quest_id
                         FROM
                           game_quest
                         WHERE
                           game_id = {self._game_id}"""
        query_results = self._db_connection.fetch(query_text, args=None)
        logger.debug(query_results)
        ids = [quest_ids[0] for quest_ids in query_results]
        return tuple(ids)

    def _get_names(self, numeri: tuple) -> tuple:
        names = []
        for quest_id in numeri:
            query_text = """SELECT
                              title
                            FROM
                              quests
                            WHERE
                              (id) = %s
                          """
            query_parameters = (quest_id,)
            query_results = self._db_connection.fetch(query_text, query_parameters)
            names.append(query_results[0][0])  # fetch ritorna una lista di tuple
        logger.debug(names)
        return tuple(names)

    def _get_times(self) -> tuple:
        query_text = f"""SELECT
                           updated_at
                         FROM
                           game_quest
                         WHERE
                           game_id = {self._game_id}"""
        query_results = self._db_connection.fetch(query_text, args=None)
        logger.debug(query_results)
        times = [quest_time[0] for quest_time in query_results]
        logger.debug(times)
        return tuple(times)

    def _get_states(self, ids: tuple) -> tuple:
        states = []
        for quest_id in ids:
            query_text = """SELECT
                              completed
                            FROM
                              game_quest
                            WHERE
                              game_id = %s AND quest_id = %s"""
            query_param = (self._game_id, quest_id)
            query_result = self._db_connection.fetchone(query_text, query_param)
            logger.debug(query_result)
            states.append(query_result[0])
        return tuple(states)

    def get_data(self) -> tuple[dict]:
        self._ids = self._get_ids()
        self._names = self._get_names(self._ids)
        self._times = self._get_times()
        self._states = self._get_states(self._ids)
        self._data = []
        zipped_quests = zip(self._ids, self._names, self._times, self._states)
        for quest in zipped_quests:
            quest_dict = {
                "quest_id": quest[0],
                "quest_name": quest[1],
                "quest_time": quest[2],
                "quest_status": quest[3],
            }
            self._data.append(quest_dict)
        self._data = tuple(self._data)
        logger.debug(self._data)
        return self._data


class PatientReport:

    reports_list = []

    def __init__(self, paziente: Patient):
        self._patient = paziente
        self._report_full_text = None

    def create_report(self) -> str:
        strings = []
        identity = f"Paziente {self._patient.__repr__()}"
        last_action = f"Ultimo cambiamento: {self._patient._last_action_at}"
        current_hero = f"Eroe attuale: {self._patient._hero}"
        coins = f"Monete attuali: {self._patient._coins}"
        n_quests = f"Quest visitate: {len(self._patient._quests._data)}"
        strings.extend([identity, last_action, current_hero, coins, n_quests])
        logger.debug(strings)
        self._report_full_text = "\n".join(strings)
        logger.debug(self._report_full_text)
        PatientReport.reports_list.append(self._report_full_text)
        return self._report_full_text

    @staticmethod
    def send_reports():
        report_timestamp = datetime.datetime.utcnow().__str__()
        reports_chain = "\n\n\n".join(PatientReport.reports_list)
        po_userkey = os.environ.get("PO_USERKEY")
        po_apptoken = os.environ.get("PO_APPTOKEN")
        body_message = f"{report_timestamp}\n\n{reports_chain}"

        request_params = {"token": po_apptoken,
                          "user": po_userkey,
                          "message": body_message
                          }
        response = requests.post("https://api.pushover.net/1/messages.json",
                                 params=request_params)
        logger.debug(response)
        return True


if __name__ == "__main__":
    logger.remove()
    logger.add(
        sys.stderr,
        format="<green>{time:YYYY-MM-DD HH:mm:ss.SSS}</green> | "
        "<level>{level: <8}</level> | "
        "<cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> - "
        "<level>{message}</level>",
        level="DEBUG",
    )
    paz1 = Patient("Beatrice D'Amico", 1000, 35)
    logger.debug(f"creata {paz1.__repr__()}")
    paz2 = Patient("Anna Barbieri", 9472, 44)
    logger.debug(f"creata {paz2.__repr__()}")
    paz3 = Patient("Diana Daniele", 9315, 47)
    logger.debug(f"creata {paz3.__repr__()}")
    paz4 = Patient("Adele Guarnieri", 9497, 46)
    logger.debug(f"creata {paz4.__repr__()}")

    paz1._quests.get_data()
    paz2._quests.get_data()
    paz3._quests.get_data()
    paz4._quests.get_data()

    rep1 = PatientReport(paz1)
    rep1.create_report()
    rep2 = PatientReport(paz2)
    rep2.create_report()
    rep3 = PatientReport(paz3)
    rep3.create_report()
    rep4 = PatientReport(paz4)
    rep4.create_report()

    PatientReport.send_reports()
